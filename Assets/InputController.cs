﻿using UnityEngine;
using System;
using System.Runtime.InteropServices;
using System.Collections;

public class InputController : MonoBehaviour {
	
	[DllImport ("DSInterface")]
	public static extern int ds_init();

	[DllImport ("DSInterface")]
	public static extern int ds_update();
	
	[DllImport ("DSInterface")]
	public static extern int ds_getGas();
	
	[DllImport ("DSInterface")]
	public static extern int ds_getBrake();
	
	[DllImport ("DSInterface")]
	public static extern int ds_getSteer();
	
	[DllImport ("DSJoyUI")]
	public static extern void ds_zero_steer();
	
	[DllImport ("DSInterface")]
	public static extern int ds_runUiThread();

	[DllImport ("DSInterface")]
	public static extern int ds_getSiren();

	[DllImport ("DSInterface")]
	public static extern int ds_getLights();


	
	
	
	
	[DllImport ("DSInterface")]
	public static extern int dsh_init();
	
	[DllImport ("DSInterface")]
	public static extern int dsh_update();
	
	[DllImport ("DSInterface")]
	public static extern float dsh_getRoll();
	
	[DllImport ("DSInterface")]
	public static extern float dsh_getPitch();
	
	[DllImport ("DSInterface")]
	public static extern float dsh_getYaw();
	
	[DllImport ("DSHeadUI")]
	public static extern void dsh_zero_heading();
	
	[DllImport ("DSInterface")]
	public static extern int dsh_runUiThread();




	[DllImport ("DSInterface")]
	public static extern int ds_getGear();




	[DllImport ("DSInterface")]
	public static extern void ds_release();

	[DllImport ("DSInterface")]
	public static extern void ds_stopUiThread();

	[DllImport ("DSInterface")]
	public static extern void dsh_release();

	[DllImport ("DSInterface")]
	public static extern void dsh_stopUiThread();


	private float sensorOffset = 60f;
	private int gearState = 65; //65-drive 63-reverse

	void Start () {
		// Initialize dll functions
		init();
		runUiThread();
		zeroOut();
	}

	void Update() {
		#if (UNITY_STANDALONE || UNITY_EDITOR)
		updateDS();
		#endif
	}
	
	public void init()
	{
		ds_init(); //Initialize dll data. Must always use ds_release() on exit
		dsh_init();
	}
	
	public void updateDS()
	{
		ds_update(); //Update values of steering, gas, brake, etc
		dsh_update(); //Update head tracker
	}
	
	public void runUiThread()
	{
		ds_runUiThread();

		if (!DS.isOVRPresent())
			dsh_runUiThread();
	}
		
	public void zeroOut()
	{
		ds_zero_steer();
		dsh_zero_heading();
	}

	public float getGas()
	{
		float gas;
		
		#if (UNITY_STANDALONE || UNITY_EDITOR)
		gas = (float) ds_getGas ();

		if(gas < 0 || gas < sensorOffset)
			gas = 0;
		
		#elif UNITY_ANDROID
		if (Input.GetMouseButton(0))
			gas = 255f;
		else
			gas = 0f;
		#endif

		if(gas > sensorOffset)
			initialized = true;
		
		return gas;
	}

	[HideInInspector] public bool initialized = false;			// Hold down brake if driver hasn't begun to drive

	public float getBrake()
	{
		float brake;

		#if (UNITY_STANDALONE || UNITY_EDITOR)
		brake = ds_getBrake ();
		
		if(brake < 0 || brake < sensorOffset)
			brake = 0;

		#elif UNITY_ANDROID
		if(Input.GetMouseButton(1))
			brake = 255f;
		else
			brake = 0f;

		#endif

		if(!initialized)
			return 255;
		else
			return brake;
	}

	public float getSteer()
	{
		//return timeSteer;
		#if (UNITY_STANDALONE || UNITY_EDITOR)
		return ds_getSteer ();

		#elif UNITY_ANDROID
		#endif

		return 0;
	}

	public int getGear()
	{
		#if (UNITY_STANDALONE || UNITY_EDITOR)
		gearState = ds_getGear ();
		#endif

		return gearState;
	}

	private int sirenState = 0;

	public int getSiren()
	{
		#if (UNITY_STANDALONE || UNITY_EDITOR)
		sirenState = ds_getSiren ();
		#endif

		return sirenState;
	}

	private int lightState = 0;

	public int getLights()
	{
		#if (UNITY_STANDALONE || UNITY_EDITOR)
		lightState = ds_getLights ();
		#endif

		return lightState;
	}

	public int getMirrorState()
	{
		int mirrorState;

		if(PlayerPrefs.HasKey("MirrorState"))
			mirrorState = PlayerPrefs.GetInt("MirrorState");		// Get mirror state
		else
			mirrorState = 0;										// If mirror was never set, set it to 0

		if(Input.GetKeyDown(KeyCode.M) && ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))))
		{
			if(mirrorState == 1)
				mirrorState = 0;
			else
				mirrorState = 1;

			PlayerPrefs.SetInt("MirrorState", mirrorState);		// Set data in registry
			PlayerPrefs.Save();									// Save data
		}

		return mirrorState;
	}


	public void release()
	{
		print ("Release");
		dsh_release();
		ds_release(); //Release on exit
	}

	public void stopUiThread()
	{
		print ("Stop UI Thread");
		dsh_stopUiThread();
		if (!DS.isOVRPresent())
		ds_stopUiThread();
	}


	/*void OnDestroy()
	{
		stopUiThread();
		release();
	}*/

	void  OnApplicationQuit()
	{
		print ("Quitting");
		stopUiThread();
		release();

		if(!Application.isEditor)
			System.Diagnostics.Process.GetCurrentProcess ().Kill ();
	}
}