﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour {

	void Start()
	{
		InvokeRepeating ("example1", 0, 1f);
		InvokeRepeating ("example2", 0, 1f);
	}

	void example1()
	{
		print ("1");
	}

	void example2()
	{
		print ("2");
	}
}
