﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

// Only used in DSCP. DSInterface looks for panels and buttons here to add
// Also handles configurations

public class DS_Cal : MonoBehaviour {

	public DSInterface dsi; // Pointer to the main DSInterface object
	public Transform PanelScrollContents;
	public GameObject CentralPeripheralButtonPrefab;
	public GameObject promptWindow;
	public Text promptText;
	private	 int promptType; // 0: zeroPosition; 1: hold gas; 2: hold break

	void Start()
	{ 
		promptWindow.SetActive (false);
		RemovePeripherals ();
	}

	public void RemovePeripherals ()
	{
		for (int i = 0; i < PanelScrollContents.childCount; ++i)
		{
			GameObject gameObject = PanelScrollContents.GetChild (i).gameObject;
			Destroy (gameObject);
		}

		//DriveSquareBLEReader[] readersList = FindObjectsOfType<DriveSquareBLEReader> ();

		foreach (DriveSquareBLEReader x in dsi.readersList) {
			x.fullDisconnect ();
		}

		BluetoothLEHardwareInterface.bluetoothDeviceScript.DiscoveredDeviceList.Clear ();
	}

	public void showPrompt(int promptType)
	{
		string message = "";

		promptWindow.SetActive (true);

		if (promptType == 0)
			message = "Release gas and brake pedals";
		else if(promptType == 1)
			message = "Press and hold gas";

		promptText.text = message;
		this.promptType = promptType;
	}

	void zeroPositions()
	{
		// DriveSquareBLEReader[] sensorList = FindObjectsOfType<DriveSquareBLEReader> ();

		foreach (DriveSquareBLEReader sensor in dsi.readersList) {

			switch (sensor.type) {
			case 1: // Steer
				sensor.turnsR = Vector3.zero;
				break;
			case 2: // Gas
			case 3: // Brake
				sensor.calZero = sensor.raw;
				break;
			}
		}
	}

	public void confirm()
	{
		switch (promptType) {
		case 0:
			zeroPositions ();
			dsi.BLE_Steer.writeConfig ();
			promptWindow.SetActive (false);
			break;
		case 1:
			dsi.BLE_Gas.SetCalibrationMultipliers ();
			dsi.BLE_Gas.writeConfig ();
			promptText.text = "Press and hold brake";
			promptType = 2;
			break;
		case 2:
			dsi.BLE_Brake.SetCalibrationMultipliers ();
			dsi.BLE_Brake.writeConfig ();
			promptWindow.SetActive (false);
			break;
		}
	}

	public void Refresh()
	{
		dsi.StopScan ();
		RemovePeripherals ();
		dsi.OnScan ();
	}
}
