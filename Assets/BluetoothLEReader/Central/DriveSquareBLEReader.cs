﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.IO;

// This script holds all the data (Calibration, Best Axis, SensorType) for InputController to read from
using System.Collections;


public class DriveSquareBLEReader : MonoBehaviour
{
	public string Address;

	string dsConfigPath = "storage/emulated/0/Android/ds_";

	string _accelerometerServiceUUID = FullUUID ("D5C0");
	string _accelerometerReadWriteUUID = FullUUID("D5C1");
	string _accelerometerConfigureUUID = FullUUID ("D5C2");

	public bool _connecting = false;
	public bool _connected = false;
	string _connectedID = null;

	const float DS_c_angle = 90.0f; // Rotation: Change angle after which next turn might be possible 90 - 179 deg
	const float DS_h_angle = 170.0f; // 180-10 deg - Hysteresis angle around the critical turn point
	const int DS_PedalMax = 255; // Max output of a pedal reading
	const int DS_Noise = 2;   // Max expected noise form the sensor
	// Max absolute allowable value of Accel axis to be counted (anything higher means we are too close to the axis of gravity and the pole)
	const int DS_AccelMax = 15826;   // 16384 * cos(15deg) = 15826 This will have a dead zone of +- 15deg around the axis max valie 
	// Max absolute range of Accel axis to be counted (a real pedal can not have a rotation of over 60 degrees)
	const int DS_AccelRange = 14189; // 16384 * sin(60deg) = 14189 This will have a max range of 60deg for any pedal. Anything beyond that will not be taken seriously

	public Vector3 raw = Vector3.zero; 	  // Raw Accelerometer read
	public Vector3 R = Vector3.zero;	  // Rotational position in Euler Angles
	public Vector3 turnsR = Vector3.zero; // How many turns was made around each axis (important for steering)

	public int type = 0; // Box/Sensor assigned function: 0-Unassigned, 1-Steer, 2-Gas, 3-Brake 

	public int bestAxis;	// Calibration Data (typically used only for pedal sensors)
	public Vector3 calZero;
	public Vector3 calRange;
	public Vector3 calScale;

	public DSInterface dsi; // Pointer to the main DSInterface object
	public bool initialized = false;
	public DriveSquarePeripheralButtonScript button; // Only used when in DSCP to update visual changes on buttons

	float bestAxisVal = 0;
	float bestAxisZeroVal = 0;
	float bestAxisScaleVal = 1;

	Vector3 orient = Vector3.zero;
	Vector3 lastOrient = Vector3.zero;

	float time;
	float pingTime = 10f;
	Text status;

	// Needs to be seperate method from Initialize Sensor because StarCoroutine needs to be called
	// as interator and variables need to be initialized regardless if dsinterface is busy connecting
	// another sensor
	public void StartInitialization()
	{
		// Button is initialized in DSInterface.
		// If software recognizes interface as DSCP (For calibration), then button will exist
		// and buttons will be updated visually. Otherwise, ignore sine we will not be changing calibrations
		// outside of DSCP

		if (button) {
			button.reader = this;
			status = button.status;
		}
			
		updateStatus ("Advertising");
		
		readConfig ();

		if (!dsi.isBusy)
			StartCoroutine (InitializeSensor());
	}

	public IEnumerator InitializeSensor()
	{
		dsi.isBusy = true;

		updateStatus ("Initialize");

		InvokeRepeating ("Initialize", 0, 1f);

		yield return new WaitUntil (() => _connected);

		CancelInvoke ("Initialize");

		InvokeRepeating ("OnAccelerometerEnable", 0, 1f);

		yield return new WaitUntil (() => _accelerometerEnabled);

		CancelInvoke ("OnAccelerometerEnable");

		initialized = true;
		dsi.isBusy = false;
		dsi.ConnectRemaining ();
	}

	void Update()
	{
		// Ping accelerometer config file to keep it alive
		if (_accelerometerEnabled && time >= pingTime) {
			SendByte (_accelerometerServiceUUID, _accelerometerConfigureUUID, 0x01, null);
			time = 0;
		}

		time += Time.deltaTime;
	}

	public bool Connected
	{
		get { return _connected; }
		set
		{
			if (_connected != value)
			{
				_connected = value;

				if (_connected)
				{
					AccelerometerEnabled =  false;
					_connecting = false;

					updateStatus ("Connected");
				}
				else
				{					
					fullDisconnect ();
				}
			}
		}
	}

	public void Initialize ()
	{
		Connected = false;

		OnConnect ();
	}

	public void disconnect (Action<string> action)
	{
		updateStatus ("Disconnected");
		if (AccelerometerEnabled)
			AccelerometerEnabled = false;

		_connectedID = null;

		if(button)
			Destroy(button.gameObject);

		dsi.readersList.Remove (this);

		BluetoothLEHardwareInterface.bluetoothDeviceScript.DiscoveredDeviceList.Remove (Address);

		BluetoothLEHardwareInterface.DisconnectPeripheral (Address, action);
	}

	// Was original OnBack but there is no back button
	public void fullDisconnect ()
	{
		disconnect ((Address) => {
			dsi.readersList.Remove (this);
			Connected = false;
			Destroy(this);
		});
	}
		
	void OnCharacteristicNotification (string deviceAddress, string characteristicUUID, byte[] bytes)
	{
		if (deviceAddress.CompareTo (Address) == 0)
		{
			if (IsEqual(characteristicUUID, _accelerometerReadWriteUUID))
			{
				if (bytes.Length >= 6)
				{
					raw.x = (short)(CharUnsignedAtOffset (bytes, 0) + (CharUnsignedAtOffset (bytes, 1) << 8));
					raw.y = (short)(CharUnsignedAtOffset (bytes, 2) + (CharUnsignedAtOffset (bytes, 3) << 8));
					raw.z = (short)(CharUnsignedAtOffset (bytes, 4) + (CharUnsignedAtOffset (bytes, 5) << 8));

					orient.x = Mathf.Atan2 (-(float)raw.y, (float)raw.z);
					orient.y = Mathf.Atan2 (-(float)raw.x, (float)raw.z);
					orient.z = Mathf.Atan2 (-(float)raw.y, (float)raw.x);

					orient *= Mathf.Rad2Deg;

					// Do multi-turn operation for Steering only
					// Clean-up the critical point with some hysteresis (dead zone)
					if ((orient.x > DS_h_angle) || (orient.x < -DS_h_angle)) {	// preserve the sign in the dead zone - keep the last one
						if (lastOrient.x > 0) { orient.x = DS_h_angle; }
						else { orient.x = -DS_h_angle; }
					}
					if ((orient.y > DS_h_angle) || (orient.y < -DS_h_angle)) {	// preserve the sign in the dead zone - keep the last one
						if (lastOrient.y > 0) { orient.y = DS_h_angle; }
						else { orient.y = -DS_h_angle; }
					}
					if ((orient.z > DS_h_angle) || (orient.z < -DS_h_angle)) {	// preserve the sign in the dead zone - keep the last one
						if (lastOrient.z > 0) { orient.z = DS_h_angle; }
						else { orient.z = -DS_h_angle; }
					}
					if ((lastOrient.x > DS_c_angle) && (orient.x < -DS_c_angle)) { turnsR.x++; }
					if ((lastOrient.x < -DS_c_angle) && (orient.x > DS_c_angle)) { turnsR.x--; }
					if ((lastOrient.y > DS_c_angle) && (orient.y < -DS_c_angle)) { turnsR.y++; }
					if ((lastOrient.y < -DS_c_angle) && (orient.y > DS_c_angle)) { turnsR.y--; }
					if ((lastOrient.z > DS_c_angle) && (orient.z < -DS_c_angle)) { turnsR.z++; }
					if ((lastOrient.z < -DS_c_angle) && (orient.z > DS_c_angle)) { turnsR.z--; }

					lastOrient = orient;
					orient += turnsR * 360f;

					R = orient;

					string xText = orient.x.ToString ("+000;-000");
					string yText = orient.y.ToString ("+000;-000");
					string zText = orient.z.ToString ("+000;-000");

					updateStatus (xText + " : " + yText + " : " + zText);
				}
			}
		}
	}

	public bool _accelerometerEnabled = false;
	private bool AccelerometerEnabled
	{
		get { return _accelerometerEnabled; }
		set
		{
			if (_accelerometerEnabled != value)
			{
				if (_accelerometerEnabled)
				{
					updateStatus ("Accelerometer Disconnecting");

					EnableFeature (_accelerometerServiceUUID, _accelerometerConfigureUUID, 0x00, (enableCharacteristicUUID) => {

						if (IsEqual (enableCharacteristicUUID, _accelerometerConfigureUUID))
						{
							BluetoothLEHardwareInterface.UnSubscribeCharacteristic (_connectedID, _accelerometerServiceUUID, _accelerometerReadWriteUUID, (characteristicUUID) => {

								_accelerometerEnabled = value;
							});
						}
					});
				}
				else
				{
					updateStatus ("Accelerometer Connecting");

					EnableFeature (_accelerometerServiceUUID, _accelerometerConfigureUUID, 0x01, (enableCharacteristicUUID) => {
						updateStatus(enableCharacteristicUUID + " == " + _accelerometerConfigureUUID);

						if (IsEqual (enableCharacteristicUUID, _accelerometerConfigureUUID))
						{
							BluetoothLEHardwareInterface.SubscribeCharacteristicWithDeviceAddress (_connectedID, _accelerometerServiceUUID, _accelerometerReadWriteUUID, (deviceAddress, characteristicUUID) => {

								_accelerometerEnabled = value;
								updateStatus ("Ready");

							}, OnCharacteristicNotification);
						}
					});
				}
			}
		}
	}

	public void OnAccelerometerEnable ()
	{
		AccelerometerEnabled = !AccelerometerEnabled;
	}

	public void OnConnect ()
	{
		turnsR = Vector3.zero;

		if (!_connecting)
		{
			if (Connected)
			{
				disconnect ((Address) => {
					// this callback is shared with the one below
					// if this disconnect method is called then both this callback
					// and the one below will get called.
					// if this method has not been called and the device automatically
					// disconnects, then the one below will be called and this one will not be
					Connected = false;
				});
			}
			else
			{
				updateStatus ("Connecting");
				BusyScript.IsBusy = true;

				BluetoothLEHardwareInterface.ConnectToPeripheral (Address, (address) => {

					_connectedID = address;
					Connected = true;
				},
					(address, serviceUUID) => {
					},
					(address, serviceUUID, characteristicUUID) => {

						if (IsEqual(serviceUUID, _accelerometerServiceUUID))
						{
							if (IsEqual(characteristicUUID, _accelerometerReadWriteUUID))
							{
								BusyScript.IsBusy = false;
							}
						}
					}, (address) => {

						// this will get called when the device disconnects
						// be aware that this will also get called when the disconnect
						// is called above. both methods get call for the same action
						// this is for backwards compatibility
						Connected = false;
					});

				_connecting = true;
			}
		}
	}

	int CharSignedAtOffset (byte[] bytes, int offset)
	{
		return ((char)bytes[offset]);
	}
	int CharUnsignedAtOffset (byte[] bytes, int offset)
	{
		return bytes[offset];
	}

	int ShortSignedAtOffset (byte[] bytes, int offset)
	{
		int lowerByte = bytes[offset];
		int upperByte = (char)(bytes[offset + 1]);
		return ((upperByte << 8) + lowerByte);
	}

	int ShortUnsignedAtOffset (byte[] bytes, int offset)
	{
		int lowerByte = bytes[offset];
		int upperByte = bytes[offset + 1];
		return ((upperByte << 8) + lowerByte);
	}

	void EnableFeature (string serviceUUID, string configurationUUID, byte configValue, Action<string> action)
	{
		SendByte (serviceUUID, configurationUUID, configValue, action);
	}

	// this will create a full UUID from a 16 bit UUID specifically for the sensortag
	static string FullUUID (string uuid)
	{
		return "88D2" + uuid + "-DED0-11E4-8830-0800200C9A66";
	}

	bool IsEqual(string uuid1, string uuid2)
	{
		if (uuid1.Length == 4)
			uuid1 = FullUUID (uuid1);
		if (uuid2.Length == 4)
			uuid2 = FullUUID (uuid2);

		return (uuid1.ToUpper().CompareTo(uuid2.ToUpper()) == 0);
	}

	void SendByte (string serviceUUID, string characteristicUUID, byte value, Action<string> action)
	{
		BluetoothLEHardwareInterface.Log(string.Format ("Sending: {0}", value));
		byte[] data = new byte[] { value };
		BluetoothLEHardwareInterface.WriteCharacteristic (_connectedID, serviceUUID, characteristicUUID, data, data.Length, action != null, action);
	}

	void SendBytes (string serviceUUID, string characteristicUUID, byte[] data, Action<string> action)
	{
		BluetoothLEHardwareInterface.Log(string.Format ("Sending: {0}", data));
		BluetoothLEHardwareInterface.WriteCharacteristic (_connectedID, serviceUUID, characteristicUUID, data, data.Length, action != null, action);
	}

	public void updateType(int x)
	{
		type = x;

		switch (x) {
		// If unassigned, make sure that none of the controllers are assigned, if they are then clear them
		case 0:
			if (dsi.BLE_Steer == this) {
				dsi.BLE_Steer = null;
				break;
			} else if (dsi.BLE_Gas == this) {
				dsi.BLE_Steer = null;
				break;
			} else if (dsi.BLE_Brake == this) {
				dsi.BLE_Steer = null;
				break;
			}
			break;
		case 1:
			dsi.BLE_Steer = this;
			break;
		case 2:
			dsi.BLE_Gas = this;
			break;
		case 3:
			dsi.BLE_Brake = this;
			break;
		}

		writeConfig ();
	}

	public void writeConfig() // Write configuration file for the current box
	{
		// Create a var that has the contents of the file.
		//   (For some reason if we write the walues right away, then the file can't increase it's size
		//   and the values get trimmed)
		string d = type + " " + bestAxis + " " +
			calZero.x + " " + calZero.y + " " + calZero.z + " " +
			calRange.x + " " + calRange.y + " " + calRange.z + " " +
			calScale.x + " " + calScale.y + " " + calScale.z ;
		
		File.WriteAllText (dsConfigPath + getAddress() + ".txt", d);
	}

	public void readConfig() // Read configuration file for the current box
	{
		// order:
		// currentAssignment(int); bestAxisNum(int), 
		// calZeroR.x(int), calZeroR.y(int), calZeroR.z(int),
		// calRangeR.x(int), calRangeR.y(int), calRangeR.z(int), 
		// calScaleR.x(float), calScaleR.y(float), calScaleR.z(float)

		// Try to read file to initiailize variables.
		// If file doesn't exist, then initialize everything as 0

		string path = dsConfigPath + getAddress() + ".txt";
		if (File.Exists (path)) {

			// READ FILE/////////////////////
			FileInfo srcFile = null;
			StreamReader config = null;

			srcFile = new FileInfo (path);
			if(srcFile != null && srcFile.Exists)
				config = srcFile.OpenText();

			string contents = config.ReadToEnd ();
			config.Close ();
			////////////////////////////////

			string[] parameters = contents.Split (' ');
			type = int.Parse(parameters [0]);
			bestAxis = int.Parse(parameters [1]);
			calZero.Set (float.Parse(parameters [2]), float.Parse(parameters [3]), float.Parse(parameters [4]));
			calRange.Set (float.Parse(parameters [5]), float.Parse(parameters [6]), float.Parse(parameters [7]));
			calScale.Set (float.Parse(parameters [8]), float.Parse(parameters [9]), float.Parse(parameters [10]));
		} else {
			type = 0;
			bestAxis = 0;
			calZero = Vector3.zero;
			calRange = Vector3.zero;
			calScale = Vector3.zero;

			writeConfig ();
		}

		if (button) {
			button.initializeType ();
		}
	}


	void updateStatus(string x)
	{
		if (status)
			status.text = x;
	}

	public string getAddress()
	{
		string[] splitAddress = Address.Split (':');
		string trimmed = "";

		foreach (string x in splitAddress) {
			trimmed += x;
		}

		return trimmed;
	}

	public int BLE_output()
	{
		int result = 0;

		switch (type) {
		case 1:
			// Steer output
			result = (int)(5.0 * R.y);
			break;
		case 2:
		case 3:
			// Gas or Brake output
			switch (bestAxis) {
			case 1: // X
				bestAxisVal = raw.x;
				bestAxisZeroVal = calZero.x;
				bestAxisScaleVal = calScale.x;
				break;
			case 2: // Y
				bestAxisVal = raw.y;
				bestAxisZeroVal = calZero.y;
				bestAxisScaleVal = calScale.y;
				break;
			case 3: // Z
				bestAxisVal = raw.z;
				bestAxisZeroVal = calZero.z;
				bestAxisScaleVal = calScale.z;
				break;
			}
			result = (int)((bestAxisVal - bestAxisZeroVal) * bestAxisScaleVal);
			break;
		}

		return result;
	}

	public void SetCalibrationMultipliers() // This is called during the Calibration when a pedal is depressed
	{
		if ((type == 2) || (type == 3)) { 	// Only work with Gas or Brake
			calRange = raw - calZero; 		// Capture the current state 
			findBoxBestAxis ();
		}
		return;
	}

	int findBoxBestAxis() { // Find the best raw axis to use for a given pedal
		int err = 0;

		// Calculate Scale
		if (calRange.x != 0 ) { // No div by 0
			calScale.x = (float)DS_PedalMax / calRange.x;
		}
		else {
			calScale.x = 0;
		}
		if (calRange.y != 0) { // No div by 0
			calScale.y = (float)DS_PedalMax / calRange.y;
		}
		else {
			calScale.y = 0;
		}
		if (calRange.z != 0) { // No div by 0
			calScale.z = (float)DS_PedalMax / calRange.z;
		}
		else {
			calScale.z = 0;
		}

		// Choose one axis with the best output
		Vector3 absRange;
		absRange.x = Mathf.Abs(calRange.x);
		absRange.y = Mathf.Abs(calRange.y);
		absRange.z = Mathf.Abs(calRange.z);

		// Shut down an axis if it's almost perpendicular to the ground (i.e. can not measure anything)
		if ((Mathf.Abs(raw.x) > DS_AccelMax) || (Mathf.Abs(calZero.x) > DS_AccelMax) || (absRange.x > DS_AccelRange)) {
			calScale.x = 0.01f;
			calRange.x = absRange.x = 1;
		}
		if ((Mathf.Abs(raw.y) > DS_AccelMax) || (Mathf.Abs(calZero.y) > DS_AccelMax) || (absRange.y > DS_AccelRange)) {
			calScale.y = 0.01f;
			calRange.y = absRange.y = 1;
		}
		if ((Mathf.Abs(raw.z) > DS_AccelMax) || (Mathf.Abs(calZero.z) > DS_AccelMax) || (absRange.z > DS_AccelRange)) {
			calScale.z = 0.01f;
			calRange.z = absRange.z = 1;
		}

		if ((absRange.x > absRange.y) && (absRange.x > absRange.z)) { // Use X axis
			if (absRange.x < (3.0 * DS_Noise)) {
				err |= 0x01; // error on X axis
			}
			// Put out our best axis (out of 3)
			bestAxis = 1;
			bestAxisZeroVal = calZero.x;
			bestAxisScaleVal = calScale.x;
		}
		if ((absRange.y > absRange.x) && (absRange.y > absRange.z)) { // Use Y axis
			if (absRange.y < (3.0 * DS_Noise)) {
				err |= 0x02; // error on Y axis
			}
			// Put out our best axis (out of 3)
			bestAxis = 2;
			bestAxisZeroVal = calZero.y;
			bestAxisScaleVal = calScale.y;
		}
		if ((absRange.z > absRange.y) && (absRange.z > absRange.x)) { // Use Z axis
			if (absRange.z < (3.0 * DS_Noise)) {
				err |= 0x04; // error on Z axis
			}
			// Put out our best axis (out of 3)
			bestAxis = 3;
			bestAxisZeroVal = calZero.z;
			bestAxisScaleVal = calScale.z;
		}

		return err;
	}
		
}
