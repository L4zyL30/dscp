﻿using UnityEngine;
using UnityEngine.UI;

public class CentralPeripheralButtonScript : MonoBehaviour
{
	public CentralRFduinoScript PanelCentralRFduino;
	public CentralTISensorTagScript PanelCentralTISensorTag;
	public CentralNordicScript PanelCentralNordic;
	public DriveSquareScript PanelCentralDriveSquare;
	public Text TextName;
	public Text TextAddress;
	
	public void OnPeripheralSelected ()
	{
		if (TextName.text.Contains ("RFduino"))
		{
			PanelCentralRFduino.Initialize (this);
			BLETestScript.Show (PanelCentralRFduino.transform);
		}
		else if (TextName.text.Contains ("SensorTag"))
		{
			PanelCentralTISensorTag.Initialize (this);
			BLETestScript.Show (PanelCentralTISensorTag.transform);
		}
		else if (TextName.text.Contains ("Adafruit Bluefruit LE"))
		{
			PanelCentralNordic.Initialize (this);
			BLETestScript.Show (PanelCentralNordic.transform);
		}
		else if (TextName.text.Contains ("Drive Square"))
		{
			PanelCentralDriveSquare.Initialize (this);
			BLETestScript.Show (PanelCentralDriveSquare.transform);
		}
	}
}
