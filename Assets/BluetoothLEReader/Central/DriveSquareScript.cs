﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;


public class DriveSquareScript : MonoBehaviour
{
	public Transform PanelCentral;
	public Text Name;
	public Text Address;
	public Text TextConnectButton;

	public GameObject PanelAccelerometer;
	public Text TextAccelerometerEnable;
	public Text TextAccelerometerX;
	public Text TextAccelerometerY;
	public Text TextAccelerometerZ;

	private string _accelerometerServiceUUID = FullUUID ("D5C0");
	private string _accelerometerReadWriteUUID = FullUUID("D5C1");
	private string _accelerometerConfigureUUID = FullUUID ("D5C2");

	private bool _connecting = false;
	private string _connectedID = null;

	bool _connected = false;

	const float c_angle = 90.0f; // Rotation: Change angle after which next turn might be possible 90 - 179 deg
	const float h_angle = 170.0f; // 180-10 deg - Hysteresis angle around the critical turn point
	Vector3 lastOrient;
	Vector3 turnsR;

	private float time;
	private float pingTime = 10f;

	void Update()
	{
		if (_accelerometerEnabled && time >= pingTime) {
			SendByte (_accelerometerServiceUUID, _accelerometerConfigureUUID, 0x01, null);
			time = 0;
		}

		time += Time.deltaTime;
	}

	bool Connected
	{
		get { return _connected; }
		set
		{
			if (_connected != value)
			{
				_connected = value;

				if (_connected)
				{
					TextConnectButton.text = "Disconnect";
					AccelerometerEnabled = true;
					_connecting = false;
				}
				else
				{
					TextConnectButton.text = "Connect";
					PanelAccelerometer.SetActive (false);
					_connectedID = null;
				}
			}
		}
			
	}

	public void Initialize (CentralPeripheralButtonScript centralPeripheralButtonScript)
	{
		Connected = false;
		PanelAccelerometer.SetActive (false);
		Name.text = centralPeripheralButtonScript.TextName.text;
		Address.text = centralPeripheralButtonScript.TextAddress.text;
		/*TextTemperatureAmbient.text = "...";
		TextTemperatureTarget.text = "...";
		TextAccelerometerX.text = "...";
		TextAccelerometerY.text = "...";
		TextAccelerometerZ.text = "...";*/

		OnConnect ();
	}

	void disconnect (Action<string> action)
	{
		if (AccelerometerEnabled)
		{
			AccelerometerEnabled = false;
		}
		else
			BluetoothLEHardwareInterface.DisconnectPeripheral (Address.text, action);
	}

	public void OnBack ()
	{
		if (Connected)
		{
			disconnect ((Address) => {

				Connected = false;
				BLETestScript.Show (PanelCentral.transform);
			});
		}
		else
			BLETestScript.Show (PanelCentral.transform);
	}


	void OnCharacteristicNotification (string deviceAddress, string characteristicUUID, byte[] bytes)
	{
		Vector3 raw, orient;

		if (deviceAddress.CompareTo (Address.text) == 0)
		{
			if (IsEqual(characteristicUUID, _accelerometerReadWriteUUID))
			{
				if (bytes.Length >= 6)
				{
					raw.x = (short)(CharUnsignedAtOffset (bytes, 0) + (CharUnsignedAtOffset (bytes, 1) << 8));
					raw.y = (short)(CharUnsignedAtOffset (bytes, 2) + (CharUnsignedAtOffset (bytes, 3) << 8));
					raw.z = (short)(CharUnsignedAtOffset (bytes, 4) + (CharUnsignedAtOffset (bytes, 5) << 8));

					orient.x = Mathf.Atan2 (-(float)raw.y, (float)raw.z);
					orient.y = Mathf.Atan2 (-(float)raw.x, (float)raw.z);
					orient.z = Mathf.Atan2 (-(float)raw.y, (float)raw.x);

					orient *= Mathf.Rad2Deg;

					// Do multi-turn operation for Steering only
					// Clean-up the critical point with some hysteresis (dead zone)
					if ((orient.x > h_angle) || (orient.x < -h_angle)) {	// preserve the sign in the dead zone - keep the last one
						if (lastOrient.x > 0) { orient.x = h_angle; }
						else { orient.x = -h_angle; }
					}
					if ((orient.y > h_angle) || (orient.y < -h_angle)) {	// preserve the sign in the dead zone - keep the last one
						if (lastOrient.y > 0) { orient.y = h_angle; }
						else { orient.y = -h_angle; }
					}
					if ((orient.z > h_angle) || (orient.z < -h_angle)) {	// preserve the sign in the dead zone - keep the last one
						if (lastOrient.z > 0) { orient.z = h_angle; }
						else { orient.z = -h_angle; }
					}
					if ((lastOrient.x > c_angle) && (orient.x < -c_angle)) { turnsR.x++; }
					if ((lastOrient.x < -c_angle) && (orient.x > c_angle)) { turnsR.x--; }
					if ((lastOrient.y > c_angle) && (orient.y < -c_angle)) { turnsR.y++; }
					if ((lastOrient.y < -c_angle) && (orient.y > c_angle)) { turnsR.y--; }
					if ((lastOrient.z > c_angle) && (orient.z < -c_angle)) { turnsR.z++; }
					if ((lastOrient.z < -c_angle) && (orient.z > c_angle)) { turnsR.z--; }


					lastOrient = orient;
					orient += turnsR * 360f;


					TextAccelerometerX.text = orient.x.ToString ("+000;-000");
					TextAccelerometerY.text = orient.y.ToString ("+000;-000");
					TextAccelerometerZ.text = orient.z.ToString ("+000;-000");

				}
			}
		}
	}
		
	private bool _accelerometerEnabled = false;
	private bool AccelerometerEnabled
	{
		get { return _accelerometerEnabled; }
		set
		{
			if (_accelerometerEnabled != value)
			{
				if (_accelerometerEnabled)
				{
					EnableFeature (_accelerometerServiceUUID, _accelerometerConfigureUUID, 0x00, (enableCharacteristicUUID) => {

						if (IsEqual (enableCharacteristicUUID, _accelerometerConfigureUUID))
						{
							BluetoothLEHardwareInterface.UnSubscribeCharacteristic (_connectedID, _accelerometerServiceUUID, _accelerometerReadWriteUUID, (characteristicUUID) => {

								TextAccelerometerEnable.text = "Enable";
								TextAccelerometerX.text = "...";
								TextAccelerometerY.text = "...";
								TextAccelerometerZ.text = "...";
								_accelerometerEnabled = value;
							});
						}
					});
				}
				else
				{
					EnableFeature (_accelerometerServiceUUID, _accelerometerConfigureUUID, 0x01, (enableCharacteristicUUID) => {

						if (IsEqual (enableCharacteristicUUID, _accelerometerConfigureUUID))
						{
							BluetoothLEHardwareInterface.SubscribeCharacteristicWithDeviceAddress (_connectedID, _accelerometerServiceUUID, _accelerometerReadWriteUUID, (deviceAddress, characteristicUUID) => {

								TextAccelerometerEnable.text = "Disable";
								_accelerometerEnabled = value;

							}, OnCharacteristicNotification);
						}
					});
				}
			}
		}
	}

	public void OnAccelerometerEnable ()
	{
		AccelerometerEnabled = !AccelerometerEnabled;
	}

	public void OnConnect ()
	{
		turnsR.x = turnsR.y = turnsR.z = 0;

		if (!_connecting)
		{
			if (Connected)
			{
				disconnect ((Address) => {
					// this callback is shared with the one below
					// if this disconnect method is called then both this callback
					// and the one below will get called.
					// if this method has not been called and the device automatically
					// disconnects, then the one below will be called and this one will not be
					Connected = false;
				});
			}
			else
			{
				BusyScript.IsBusy = true;

				BluetoothLEHardwareInterface.ConnectToPeripheral (Address.text, (address) => {

					_connectedID = address;
					Connected = true;
				},
					(address, serviceUUID) => {
					},
					(address, serviceUUID, characteristicUUID) => {

						if (IsEqual(serviceUUID, _accelerometerServiceUUID))
						{
							if (IsEqual(characteristicUUID, _accelerometerReadWriteUUID))
							{
								PanelAccelerometer.SetActive (true);
								BusyScript.IsBusy = false;
							}
						}
					}, (address) => {

						// this will get called when the device disconnects
						// be aware that this will also get called when the disconnect
						// is called above. both methods get call for the same action
						// this is for backwards compatibility
						Connected = false;
					});

				_connecting = true;
			}
		}
	}
		
	int CharSignedAtOffset (byte[] bytes, int offset)
	{
		return ((char)bytes[offset]);
	}
	int CharUnsignedAtOffset (byte[] bytes, int offset)
	{
		return bytes[offset];
	}

	int ShortSignedAtOffset (byte[] bytes, int offset)
	{
		int lowerByte = bytes[offset];
		int upperByte = (char)(bytes[offset + 1]);
		return ((upperByte << 8) + lowerByte);
	}

	int ShortUnsignedAtOffset (byte[] bytes, int offset)
	{
		int lowerByte = bytes[offset];
		int upperByte = bytes[offset + 1];
		return ((upperByte << 8) + lowerByte);
	}

	void EnableFeature (string serviceUUID, string configurationUUID, byte configValue, Action<string> action)
	{
		SendByte (serviceUUID, configurationUUID, configValue, action);
	}

	// this will create a full UUID from a 16 bit UUID specifically for the sensortag
	static string FullUUID (string uuid)
	{
		return "88D2" + uuid + "-DED0-11E4-8830-0800200C9A66";
	}

	bool IsEqual(string uuid1, string uuid2)
	{
		if (uuid1.Length == 4)
			uuid1 = FullUUID (uuid1);
		if (uuid2.Length == 4)
			uuid2 = FullUUID (uuid2);

		return (uuid1.ToUpper().CompareTo(uuid2.ToUpper()) == 0);
	}

	void SendByte (string serviceUUID, string characteristicUUID, byte value, Action<string> action)
	{
		BluetoothLEHardwareInterface.Log(string.Format ("Sending: {0}", value));
		byte[] data = new byte[] { value };
		BluetoothLEHardwareInterface.WriteCharacteristic (_connectedID, serviceUUID, characteristicUUID, data, data.Length, action != null, action);
	}

	void SendBytes (string serviceUUID, string characteristicUUID, byte[] data, Action<string> action)
	{
		BluetoothLEHardwareInterface.Log(string.Format ("Sending: {0}", data));
		BluetoothLEHardwareInterface.WriteCharacteristic (_connectedID, serviceUUID, characteristicUUID, data, data.Length, action != null, action);
	}
}
