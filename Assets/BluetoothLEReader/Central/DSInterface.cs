﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;


// Scans and connects BLEReaders
// Located on GlobalOBJ in case any sensors drop out so this can pick it back up during scan

public class DSInterface : MonoBehaviour
{
	private DS_Cal calibration;
	private bool _scanning = false;
	public bool isBusy;
	public string path;

	public List<DriveSquareBLEReader> readersList = new List<DriveSquareBLEReader>();

	public DriveSquareBLEReader BLE_Steer;
	public DriveSquareBLEReader BLE_Gas;
	public DriveSquareBLEReader BLE_Brake;

	void Start ()
	{
		Initialize ();
	}

	public void Initialize ()
	{
		calibration = FindObjectOfType<DS_Cal> ();
		calibration.dsi = this;

		BluetoothLEHardwareInterface.Initialize (true, false, () => {
			
		}, (error) => {
		});

		OnScan ();
	}

	protected string BytesToString (byte[] bytes)
	{
		string result = "";

		foreach (var b in bytes)
			result += b.ToString ("X2");

		return result;
	}

	public void StopScan()
	{
		BluetoothLEHardwareInterface.StopScan ();
		_scanning = false;
	}

	public void OnScan ()
	{
		isBusy = false;

		// the first callback will only get called the first time this device is seen
		// this is because it gets added to a list in the BluetoothDeviceScript
		// after that only the second callback will get called and only if there is
		// advertising data available

		BluetoothLEHardwareInterface.ScanForPeripheralsWithServices (null, (address, name) => {

			if(name.Contains("Drive Square"))
			{
				AddPeripheral (name, address);
			}
			

		}, (address, name, rssi, advertisingInfo) => {

			if (advertisingInfo != null)
			{
				BluetoothLEHardwareInterface.Log (string.Format ("Device: {0} RSSI: {1} Data Length: {2} Bytes: {3}", name, rssi, advertisingInfo.Length, BytesToString (advertisingInfo)));
			}
		});

		_scanning = true;
	}

	void AddPeripheral (string name, string address)
	{
		/*
		// If a type already exists in the current roll out, cancel this method and do not instantiate a new button
		if (_peripheralList.ContainsKey (address)) {
			
			DriveSquarePeripheralButtonScript checkScript = new DriveSquarePeripheralButtonScript();

			for (int i = 0; i < PanelScrollContents.childCount; ++i) {
				
				DriveSquareBLEReader reader = FindObjectsOfType<DriveSquareBLEReader> ();

				if (reader.type == readConfig(_peripheralList[address], checkScript).type)
					return;
			}
		}
		*/

		DriveSquareBLEReader reader = gameObject.AddComponent<DriveSquareBLEReader>();

		reader.name = name;
		reader.Address = address;
		reader.dsi = this;

		if (calibration) {
			GameObject peripheralObject = (GameObject)Instantiate (calibration.CentralPeripheralButtonPrefab);
			DriveSquarePeripheralButtonScript script = peripheralObject.GetComponent<DriveSquarePeripheralButtonScript> ();
			script.TextName.text = name;
			script.TextAddress.text = address;
			peripheralObject.transform.SetParent (calibration.PanelScrollContents);
			peripheralObject.transform.localScale = new Vector3 (1f, 1f, 1f);
			reader.button = script;
		}

		reader.StartInitialization ();
		readersList.Add(reader);

		/*if (_peripheralList.ContainsKey (address))
			readConfig(_peripheralList[address], script);

		script.initializeType ();*/
	}

	public void ConnectRemaining()
	{
		// Find all BLEReaders and check if they have been initialized
		//DriveSquareBLEReader[] readers = FindObjectsOfType<DriveSquareBLEReader> ();

		foreach (DriveSquareBLEReader x in readersList) {
			if(!x.initialized) {
				StartCoroutine (x.InitializeSensor ());
				break;
			}
		}
	}

	public int get_Steer()
	{
		return BLE_Steer.BLE_output ();
	}

	public int get_Gas()
	{
		return BLE_Gas.BLE_output ();
	}

	public int get_Brake()
	{
		return BLE_Brake.BLE_output ();
	}

	public void Update () { // Check the output results
		DS.logText ("Steer:" + get_Steer() + " Brake:" + get_Brake() + " Gas:" + get_Gas() + "        ");
	}
}
