﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

// This is a visual representation of the data being read from BLEReader.
//Only for buttons setting variables for BLEReader so it knows what type Sensor is (Brake, Gas, Steer)

public class DriveSquarePeripheralButtonScript : MonoBehaviour
{
	public Text status;
	public Text TextName;
	public Text TextAddress;
	public DriveSquareBLEReader reader;
	private bool connected;

	public Button Brake;
	public Button Gas;
	public Button Steer;

	public void initializeType()
	{
		switch (reader.type) {
		case 0:
			DriveSquareBLEReader[] otherReaders = FindObjectsOfType<DriveSquareBLEReader> ();

			for (int i = 0; i < otherReaders.Length; i++) {
				if (otherReaders [i].type == 1)
					Steer.interactable = false;
				if (otherReaders [i].type == 2)
					Gas.interactable = false;
				if (otherReaders [i].type == 3)
					Brake.interactable = false;
			}
			break;

		case 1:
			OnSteer ();
			break;

		case 2:
			OnGas ();
			break;

		case 3:
			OnBrake ();
			break;
		}
	}

	/*#define BOX_UNASSIGNED 0
	#define BOX_STEER 1
	#define BOX_GAS 2
	#define BOX_BRAKE 3
	#define BOX_CLUTCH 4
	#define BOX_GEAR 5
	#define BOX_EVO 6
	#define BOX_HEAD 7*/

	public void OnBrake()
	{
		reader.updateType (3);
		ToggleButtons (Gas, Steer);
	}

	public void OnGas()
	{
		reader.updateType (2);
		ToggleButtons (Steer, Brake);
	}

	public void OnSteer()
	{
		reader.updateType (1);
		ToggleButtons (Gas, Brake);
	}

	void ToggleButtons(Button x, Button y)
	{
		x.gameObject.SetActive (!x.gameObject.activeSelf);
		y.gameObject.SetActive (!y.gameObject.activeSelf);

		// Get current state of the other buttons to decide what to do with all other buttons
		bool boolState = x.gameObject.activeSelf;

		DriveSquarePeripheralButtonScript[] buttonList = FindObjectsOfType<DriveSquarePeripheralButtonScript> ();

		// For example: If Gas is selected, the Brake and Steer button will become hidden
		// and the other buttons's Gas button will become unable to be selected so we don't get
		// a duplicate
		foreach(DriveSquarePeripheralButtonScript i in buttonList) {
			if (i != this) {
				switch (reader.type) {
				case 0:
					break;
				case 1:
					i.Steer.interactable = boolState;
					break;
				case 2:
					i.Gas.interactable = boolState;
					break;
				case 3:
					i.Brake.interactable = boolState;
					break;
				}
			}
		}

		// If the button is currently active, then nothing is selected
		// Set BLE state to unassigned
		if (boolState)
			reader.updateType (0);
	}

	public void OnDisconnect()
	{
		reader.Connected = false;
		reader.OnConnect ();
	}
}
