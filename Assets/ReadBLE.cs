﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadBLE : MonoBehaviour {

	[SerializeField] private Text s;
	private InputController c;

	void Start()
	{
		c = DS.FindGlobalObject ().GetComponent<InputController> ();
	}

	void Update()
	{
		s.text = c.getGas ().ToString();
	}
}
