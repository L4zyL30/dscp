﻿// This class is meant to hold custom static functions

using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DS : MonoBehaviour {

	// Traverse up hierarchy one at a time to find a parent with the given tag
	public static bool ParentWithTagExists(GameObject childObject, string tag)
	{
		Transform t = childObject.transform;
		while (t.parent != null)
		{
			if (t.parent.tag == tag)
			{
				return true;
			}
			t = t.parent.transform;
		}
		return false; // Could not find a parent with given tag.
	}

	public static GameObject FindGlobalObject()
	{
		return FindObjectOfType<Original>().gameObject;
	}

	public static StreamReader ReadFile(string filePath)
	{
		FileInfo srcFile = null;
		StreamReader reader = null;

		srcFile = new FileInfo (Application.dataPath + "/StreamingAssets/" + filePath);
		if(srcFile != null && srcFile.Exists)
			reader = srcFile.OpenText();

		return reader;
	}

	public static void OVR_Recenter()
	{
		UnityEngine.VR.InputTracking.Recenter();
	}

	public static bool isOVRPresent()
	{
		return UnityEngine.VR.VRDevice.isPresent;
	}

	public static bool onMainMenu()
	{
		return SceneManager.GetActiveScene ().name == "MainMenu";
	}

	public static void logText (string x)
	{
		FindObjectOfType<logText> ().GetComponent<Text> ().text = x;
	}
}
